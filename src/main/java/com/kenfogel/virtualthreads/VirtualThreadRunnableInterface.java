package com.kenfogel.virtualthreads;

/**
  * Here is a class whose run method just counts down from the initial value of
 * actionCounter. It should display the thread name and current value for
 * actionCounter every time in the while(true) loop. I have made the counter a
 * local variable in the run method and did all the print output in run without
 * toString. It made no difference. This code as native threads works as
 * expected.
 *
 * @author omniprof
 */
public class VirtualThreadRunnableInterface implements Runnable {

    private int actionCounter = 25;

    @Override
    public String toString() {
        return "#" + Thread.currentThread().getName() + " : " + actionCounter;
    }

    /**
     * This method works as expected with a native thread but not a virtual thread
     */
    @Override
    public void run() {

        while (true) {
            System.out.printf("%s%n", this);
            if (--actionCounter == 0) {
                return;
            }
        }
    }
}
