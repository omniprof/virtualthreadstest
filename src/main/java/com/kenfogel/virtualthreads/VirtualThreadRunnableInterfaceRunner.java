package com.kenfogel.virtualthreads;

/**
 * Simple example of Virtual Threads that does not work as expected
 *
 * @author omniprof
 */
public class VirtualThreadRunnableInterfaceRunner {

    /**
     * As this code is written the virtual threads do not appear to work. The
     * program ends immediately. If you un-comment the four lines in this method
     * then the threads appear to work sometimes. What I mean is that some
     * expected output does not happen. You may need to run this a few times to
     * see this effect. If you leave the four lines commented out but increase
     * the for loop to 5000 you get some output but threads appear to be
     * missing.
     */
    public void perform() {
//        long start = System.currentTimeMillis();
        for (int i = 0; i < 5; ++i) { // increase to 5000 to get some but not all output
            Thread.ofVirtual().name("Thread # " + i).start(new VirtualThreadRunnableInterface());
            // Works if native thread is used
            // new Thread(new VirtualThreadRunnableInterface(), "" + ++i).start();
        }
//        long finish = System.currentTimeMillis();
//        long timeElapsed = finish - start;
//        System.out.println("Run time: " + timeElapsed);
    }

    public static void main(String[] args) {
        new VirtualThreadRunnableInterfaceRunner().perform();
    }
}
